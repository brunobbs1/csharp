﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cadastro_Usuario
{
    #region Pessoa
    public class Pessoa
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Endereco { get; set; }
        public string Estado { get; set; }

        public Pessoa(int id, string nome, string telefone, string email, string endereco, string estado)
        {
            ID = id;
            Nome = nome;
            Telefone = telefone;
            Email = email;
            Endereco = endereco;
            Estado = estado;
        }
    }
    #endregion

    #region Pessoa Juridica
    public class PessoaJuridica : Pessoa
    {
        public string CNPJ { get; set; }
        public string RazaoSocial { get; set; }

        public PessoaJuridica(int id, string nome, string telefone, string email, string endereco, string estado, string cnpj, string razaosocial)
            : base(id, nome, telefone, email, endereco, estado)
        {
            CNPJ = cnpj;
            RazaoSocial = razaosocial;
        }
    }
    #endregion

    #region Pessoa Fisica
    public class PessoaFisica : Pessoa
    {
        public string CPF { get; set; }

        public PessoaFisica(int id, string nome, string telefone, string email, string endereco, string estado, string cpf)
            : base(id, nome, telefone, email, endereco, estado)
        {
            CPF = cpf;
        }
    }
    #endregion

    class Program
    {
        static void Main(string[] args)
        {
            // Lista de Pessoa Juridica
            var ListaPJ = new List<PessoaJuridica>()
            {
                new PessoaJuridica(1,"Coca Cola","19 11223344","contato@cocacola.com","Beach Park ST104","Miami","09.080.276/0001-00","Coca Cola S.A"),
                new PessoaJuridica(2,"Vale","19 22334455","contato@vale.com","Rua lorem 103","São Paulo","33.320.881/0001-87","Vale S.A"),
                new PessoaJuridica(3,"Engie","19 33445566","contato@engie.com","Rua ipsum 99","Rio de Janeiro","01.670.417/0001-24","Engie S.A"),
                new PessoaJuridica(4,"Raizen","19 44556677","contato@raizen.com","Rua voluptate  22","São Paulo","04.962.633/0001-31","Raizen S.A"),
                new PessoaJuridica(5,"Claro","19 55667788","contato@claro.com","Rua cillum  495","São Paulo","50.342.737/0001-60","Claro S.A")
            };
            // Lista de Pessoa Fisica
            var ListaPF = new List<PessoaFisica>()
            {
                new PessoaFisica(1,"José","19 99999999","jose@hotmail.com","Rua do sertão 333", "Acre","478.654.590-25"),
                new PessoaFisica(2,"Tonhão","19 88888888","jose@hotmail.com","Rua do sertão 333", "São Paulo","848.857.230-19"),
                new PessoaFisica(3,"Gabriel","19 77777777","jose@hotmail.com","Rua do sertão 333", "São Paulo","124.529.790-23"),
                new PessoaFisica(4,"Claudinete","19 66666666","jose@hotmail.com","Rua do sertão 333", "São Paulo","615.861.850-04"),
                new PessoaFisica(5,"Marcelino","19 55555555","jose@hotmail.com","Rua do sertão 333", "São Paulo","954.802.290-79")
            };

            Console.WriteLine("Escolha o numero do exercicio:");
            Console.WriteLine();
            Console.WriteLine("1) Liste somente um pessoa PJ, filtrando pelo CNPJ");
            Console.WriteLine("2) Liste somente um pessoa PF, filtrando pelo CPF");
            Console.WriteLine("3) Liste todos PJ.");
            Console.WriteLine("4) Liste todos PF.");
            Console.WriteLine("5) Remova um PJ da lista, e faça a listagem.");
            Console.WriteLine("6) Remova um PF da lista, e faça a listagem.");
            Console.WriteLine("7) Atualize um PJ, e faça a listagem.");
            Console.WriteLine("8) Atualize um PF, e faça a listagem.");
            Console.WriteLine();
            int exercicio = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
            switch (exercicio)
            {
                case 1:
                    PrintOnePJ(ListaPJ);
                    break;
                case 2:
                    PrintOnePF(ListaPF);
                    break;
                case 3:
                    GetAllPJ(ListaPJ);
                    break;
                case 4:
                    GetAllPF(ListaPF);
                    break;
                case 5:
                    DeletePJ(ListaPJ);
                    break;
                case 6:
                    DeletePF(ListaPF);
                    break;
                case 7:
                    UpdatePJ(ListaPJ);
                    break;
                case 8:
                    UpdatePF(ListaPF);
                    break;
            }
            
        }
        static void PrintPF(PessoaFisica pessoaFisica)
        {
            Console.WriteLine("ID: {0}", pessoaFisica.ID);
            Console.WriteLine("Nome: {0}", pessoaFisica.Nome);
            Console.WriteLine("Telefone: {0}", pessoaFisica.Telefone);
            Console.WriteLine("Email: {0}", pessoaFisica.Email);
            Console.WriteLine("Endereco: {0}", pessoaFisica.Endereco);
            Console.WriteLine("Estado: {0}", pessoaFisica.Estado);
            Console.WriteLine("CPF: {0}", pessoaFisica.CPF);
            Console.ReadKey();
        }
        static void PrintPJ(PessoaJuridica pessoaJuridica)
        {
            Console.WriteLine("ID: {0}", pessoaJuridica.ID);
            Console.WriteLine("Nome: {0}", pessoaJuridica.Nome);
            Console.WriteLine("Telefone: {0}", pessoaJuridica.Telefone);
            Console.WriteLine("Email: {0}", pessoaJuridica.Email);
            Console.WriteLine("Endereco: {0}", pessoaJuridica.Endereco);
            Console.WriteLine("Estado: {0}", pessoaJuridica.Estado);
            Console.WriteLine("CNPJ: {0}", pessoaJuridica.CNPJ);
            Console.WriteLine("RazaoSocial: {0}", pessoaJuridica.RazaoSocial);
            Console.ReadKey();
        }
        // Primeiro
        static void PrintOnePJ(List<PessoaJuridica> pessoaJuridica)
        {
            var pj = pessoaJuridica.Where(p => p.CNPJ == "04.962.633/0001-31").FirstOrDefault();

            if (pj != null)
            {
                PrintPJ(pj);
            }
        }
        // Segundo
        static void PrintOnePF(List<PessoaFisica> pessoaFisica)
        {
            var pf = pessoaFisica.Where(p => p.CPF == "848.857.230-19").FirstOrDefault();

            if (pf != null)
            {
                PrintPF(pf);
            }
        }
        // Terceiro
        static void GetAllPJ(List<PessoaJuridica> pessoaJuridica)
        {
            foreach(var pj in pessoaJuridica)
            {
                Console.WriteLine("ID: {0}", pj.ID);
                Console.WriteLine("Nome: {0}", pj.Nome);
                Console.WriteLine("Telefone: {0}", pj.Telefone);
                Console.WriteLine("Email: {0}", pj.Email);
                Console.WriteLine("Endereco: {0}", pj.Endereco);
                Console.WriteLine("Estado: {0}", pj.Estado);
                Console.WriteLine("CNPJ: {0}", pj.CNPJ);
                Console.WriteLine("RazaoSocial: {0}", pj.RazaoSocial);
                Console.WriteLine();
            }
            Console.ReadKey();
        }
        // Quarto
        static void GetAllPF(List<PessoaFisica> pessoaFisica)
        {
            foreach (var pf in pessoaFisica)
            {
                Console.WriteLine("ID: {0}", pf.ID);
                Console.WriteLine("Nome: {0}", pf.Nome);
                Console.WriteLine("Telefone: {0}", pf.Telefone);
                Console.WriteLine("Email: {0}", pf.Email);
                Console.WriteLine("Endereco: {0}", pf.Endereco);
                Console.WriteLine("Estado: {0}", pf.Estado);
                Console.WriteLine("CNPJ: {0}", pf.CPF);
                Console.WriteLine();
            }
            Console.ReadKey();
        }
        // Quinto
        static void DeletePJ(List<PessoaJuridica> pessoaJuridica)
        {
            pessoaJuridica.RemoveAt(3);
            GetAllPJ(pessoaJuridica);
        }
        // Sexto
        static void DeletePF(List<PessoaFisica> pessoaFisica)
        {
            var remove = pessoaFisica.Where(p => p.ID == 3).SingleOrDefault();
            pessoaFisica.Remove(remove);
            GetAllPF(pessoaFisica);
        }
        // Setimo
        static void UpdatePJ(List<PessoaJuridica> pessoaJuridica)
        {
            var pj = pessoaJuridica.Where(p => p.ID == 1).SingleOrDefault();
            pj.Nome = "Novo Nome";
            GetAllPJ(pessoaJuridica);
        }
        // Oitavo
        static void UpdatePF(List<PessoaFisica> pessoaFisica)
        {
            var pf = pessoaFisica.Where(p => p.ID == 3).SingleOrDefault();
            pf.Nome = "Novo Nome";
            pf.Telefone = "123";
            GetAllPF(pessoaFisica);
        }
    }
}
