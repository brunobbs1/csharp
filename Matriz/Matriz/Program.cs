﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matriz
{
    class Program
    {
        static void Main(string[] args)
        {
            var p = new Program();

            int[,] matriz1 = new int[2, 2];
            int[,] matriz2 = new int[2, 2];
            int[,] matriz3 = new int[2, 2];

            matriz1[0, 0] = 1;
            matriz1[0, 1] = 2;
            matriz1[1, 0] = 3;
            matriz1[1, 1] = 4;

            matriz2[0, 0] = 2;
            matriz2[0, 1] = 4;
            matriz2[1, 0] = 6;
            matriz2[1, 1] = 8;

            //p.ListarMatriz(matriz1);
            //p.ExibirDiagonalPrincipal(matriz2);
            //p.SomarMatriz(matriz1, matriz2, matriz3);
            p.ExibirValor(matriz1);

            Console.ReadKey();
        }
        // 1
        public void ListarMatriz(int[,] matriz)
        {
            for (int i = 0; i < matriz.GetLength(0); i++)
            {
                for (int j = 0; j < matriz.GetLength(0); j++)
                {
                    Console.WriteLine("X:{0} Y:{1} = {2}", i, j, matriz[i, j]);
                }
            }
        }
        // 2
        public void ExibirDiagonalPrincipal(int[,] matriz)
        {

        }
        // 3
        public void ExibirDiagonalSecundaria()
        {

        }
        // 4
        public void SomarMatriz(int[,] matriz1, int[,] matriz2, int[,] matriz3)
        {
            for (int i = 0; i < matriz3.GetLength(0); i++)
            {
                for (int j = 0; j < matriz3.GetLength(0); j++)
                {
                    matriz3[i, j] = matriz1[i, j] + matriz2[i, j];
                    Console.WriteLine("X:{0} Y:{1} = {2}", i, j, matriz3[i, j]);
                }
            }
        }
        // 5
        public void Multiplicar(int[,] matriz1, int[,] matriz2, int[,] matriz3)
        {
            for (int i = 0; i < matriz3.GetLength(0); i++)
            {
                for (int j = 0; j < matriz3.GetLength(0); j++)
                {
                    matriz3[i, j] = matriz1[i, j] * matriz2[i, j];
                    Console.WriteLine("X:{0} Y:{1} = {2}", i, j, matriz3[i, j]);
                }
            }
        }
        // 5
        public void Subitrair(int[,] matriz1, int[,] matriz2, int[,] matriz3)
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    matriz3[i, j] = matriz1[i, j] - matriz2[i, j];
                    Console.WriteLine("X:{0} Y:{1} = {2}", i, j, matriz3[i, j]);
                }
            }
        }
        // 6
        public void ExibirValor(int[,] matriz1)
        {
            Console.WriteLine("Eixo X digite 1  Eixo Y digite 2: ");
            int eixo = int.Parse(Console.ReadLine());

            if (eixo == 1 || eixo == 2)
            {
                Console.WriteLine("Entre com a posição desejada: ");
                int posicao = int.Parse(Console.ReadLine());

                if (posicao == 0 || posicao == 1)
                    Console.WriteLine("Você escolheu o Eixo {0} na posição {1} o valor é: {2}", eixo, posicao, matriz1[eixo, posicao]);
                else
                    Console.WriteLine("Posição invalido ");
            }
            else
                Console.WriteLine("Eixo invalido ");
        }
    }
}
